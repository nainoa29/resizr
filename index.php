<?php
use Slim\Factory\AppFactory;
use App\Services\Storage\S3StorageProvider;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require __DIR__ . '/vendor/autoload.php';

$eventBridgeClient = new \Aws\EventBridge\EventBridgeClient([
    'region' => 'us-west-2',
    'version' => 'latest'
]);

$app = AppFactory::create();

$s3Client = new \Aws\S3\S3Client([
    'region' => getenv('TMARKS_DEFAULT_REGION'),
    'version' => '2006-03-01',
]);

$storageProvider = S3StorageProvider::create($s3Client, getenv('TMARKS_BUCKET'));

$app->post('/upload', function (Request $request, Response $response) use ($storageProvider){
    $request = $request->withAttribute('action', 'upload');
    $controller = new \App\Services\Controller($response, $storageProvider);
    return $controller->handle($request);
});

$app->run();

