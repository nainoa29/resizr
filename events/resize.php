<?php declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Bref\Context\Context;
use Bref\Event\S3\S3Event;
use Bref\Event\S3\S3Handler;

class Handler extends S3Handler
{
    public function handleS3(S3Event $event, Context $context): void
    {
        try {
            $logger = new \Bref\Logger\StderrLogger();
            $bucketName = $event->getRecords()[0]->getBucket()->getName();
            $fileName = $event->getRecords()[0]->getObject()->getKey();

            $logger->info('INCOMING KEY: ' . $fileName);

            $s3Client = new \Aws\S3\S3Client([
                'region' => getenv('TMARKS_DEFAULT_REGION'),
                'version' => '2006-03-01',
            ]);

            $resizeImage = \App\Intents\ResizeImage::create($s3Client, $bucketName, $fileName);
            $resizeImage->run();
        } catch(Throwable $throwable) {
            $logger->error('TMARKS PUT OBJECT EXCEPTION: ' . $throwable->getMessage());
        }
    }
}

return new Handler();