<?php
namespace App\Requests;

interface RequestModelInterface{

    /**
     * @param array $request
     * @return mixed
     */
    public function parse(array $request);

    /**
     * @return array
     */
    public function toArray() : array;

}