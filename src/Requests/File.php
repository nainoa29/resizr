<?php

namespace App\Requests;

use Psr\Http\Message\ServerRequestInterface;

class File implements RequestModelInterface {

    protected $originalName;
    protected $content;
    protected $type;

    public function setOriginalName($originalName) {
        $this->originalName = $originalName;
        return $this;
    }

    public function getOriginalName() {
        return $this->originalName;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getType() {
        $parts = explode('/', $this->type);
        return strtolower(end($parts));
    }

    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function getContent() {
        return $this->content;
    }

    public function parse(array $request)
    {
        $tmpLocation = $request['tmp_name'];
        $contents = file_get_contents($tmpLocation);

        $this->setOriginalName($request['name'])
                ->setContent($contents)
                ->setType($request['type']);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
          'content' => $this->content
        ];
    }

    public static function create()
    {
        return new static;
    }
}