<?php

namespace App\Intents;

use App\Requests\File;
use App\Services\Storage\StorageEntry;
use App\Services\Storage\StorageProviderInterface;

class SaveImage {

    protected $storageProvider;
    protected $fileModel;

    const TYPE_JPEG = 'jpeg';
    const TYPE_JPG = 'jpg';
    const TYPE_PNG = 'png';
    const TYPE_GIF = 'gif';

    /**
     * SaveImage constructor.
     * @param StorageProviderInterface $storageProvider
     * @param File $fileModel
     */
    public function __construct( StorageProviderInterface $storageProvider, File $fileModel ) {
        $this->storageProvider = $storageProvider;
        $this->fileModel = $fileModel;
    }

    /**
     * @return File
     */
    public function getFileModel() {
        return $this->fileModel;
    }

    /**
     * @return StorageProviderInterface
     */
    public function getStorageProvider() {
        return $this->storageProvider;
    }

    public function run() {
        try {
            $this->validate();
            $storageEntry = $this->buildStorageEntry();

            $this->storeObject($storageEntry);
        } catch(\Throwable $throwable) {
            throw $throwable;
        }
    }

    public function validate() {
        if(!in_array($this->getFileModel()->getType(), [self::TYPE_GIF, self::TYPE_JPEG, self::TYPE_JPG, self::TYPE_PNG])) {
            throw new \Exception('Invalid image type. Must be of type ' . implode(',', [self::TYPE_GIF, self::TYPE_JPEG, self::TYPE_JPG, self::TYPE_PNG]));
        }
    }

    /**
     * @return StorageEntry
     */
    public function buildStorageEntry() {
        $storageEntry = StorageEntry::create();
        $storageEntry->parse([
           'content' => $this->getFileModel()->getContent(),
           'contentType' => 'image/' . $this->getFileModel()->getType(),
           'key' => $this->buildStorageKey()
        ]);

        return $storageEntry;
    }

    public function buildStorageKey() {
        return uniqid(rand(36)) . '.' . $this->getFileModel()->getType();
    }

    public function storeObject($storageEntry) {
        try {
            $this->getStorageProvider()->putObject($storageEntry);
        }catch(\Throwable $objThrowable) {
            throw $objThrowable;
        }
    }

    /**
     * @param $storageProvider
     * @param $fileModel
     * @return static
     */
    public static function create($storageProvider, $fileModel) {
        return new static($storageProvider, $fileModel);
    }

}