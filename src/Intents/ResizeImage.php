<?php

namespace App\Intents;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use App\Libraries\FileProcessing\FileProcessorFactory;

class ResizeImage {

    protected $s3Client;
    protected $bucket;
    protected $key;

    const TMP_PATH = '/tmp/image/';
    const RESIZE_PERCENT = 0.5;

    /**
     * ResizeImage constructor.
     * @param S3Client $s3Client
     * @param $bucket
     * @param $key
     */
    public function __construct(S3Client $s3Client, $bucket, $key) {
        $this->s3Client = $s3Client;
        $this->bucket = $bucket;
        $this->key = $key;
    }

    public function run() {
        try {
            if( !$this->validateIncomingObject() ) {
                return true;
            }

            $s3Object = $this->loadContent();

            $this->resize($s3Object);

            $this->putThumbnail();
        } catch(\Throwable $throwable) {
            die('Exception ' . $throwable->getMessage());
        }
    }

    public function validateIncomingObject() {
        /*
         * Check if key includes backslash, this means the key is stored
         * in a subdirectory, we only want to resize images coming into the main bucket
         */
        $test = preg_match("/\//", $this->key);

        if($test == 1) {
            return false;
        }

        return true;
    }

    public function loadContent() {
       if(!is_dir('/tmp/image/')) {
           mkdir( '/tmp/image/');
           mkdir( '/tmp/image/resized/');
       }

       try {
           $s3Object = $this->s3Client->getObject([
               'Bucket' => 'tmarks-media',
               'Key' => $this->key,
               'SaveAs' => self::TMP_PATH . $this->key
           ]);
       } catch (S3Exception $s3Exception) {
           throw $s3Exception;
       }

       return $s3Object;
    }

    public function resize($s3Object) {
        $fileName = self::TMP_PATH . $this->key;
        $contentType = $s3Object['ContentType'];

        $fileProcessor = FileProcessorFactory::create()->get($contentType, $fileName, self::TMP_PATH . 'resized/' . $this->key);

        try {
            $fileProcessor->process();
        } catch(\Throwable $throwable) {
            throw $throwable;
        }
    }

    public function putThumbnail() {
        $this->s3Client->putObject([
            'Key' => 'thumbnails/' . $this->key,
            'SourceFile' => self::TMP_PATH . 'resized/' . $this->key,
            'Bucket' => 'tmarks-media'
        ]);
    }

    public static function create($s3Client, $bucket, $key) {
        return new static($s3Client, $bucket, $key);
    }

}