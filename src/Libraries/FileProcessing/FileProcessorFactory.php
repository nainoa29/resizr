<?php

namespace App\Libraries\FileProcessing;

class FileProcessorFactory {

    const JPEG = 'image/jpeg';
    const JPG = 'image/jpeg';
    const GIF = 'image/gif';
    const PNG = 'image/png';

    public function get($contentType, $sourceFilePath, $targetPath) {
        switch ($contentType) {
            case self::JPEG:
            case self::JPG:
                return new JpegProcessor($sourceFilePath, $targetPath);

            case self::GIF:
                return new GifProcessor($sourceFilePath, $targetPath);

            case self::PNG:
                return new PngProcessor($sourceFilePath, $targetPath);
        }
    }

    public static function create() {
        return new static;
    }
}