<?php

namespace App\Libraries\FileProcessing;

class JpegProcessor extends FileProcessor {

    /**
     * @return false|\GdImage|resource
     */
    protected function createSource()
    {
        return imagecreatefromjpeg($this->getSourceFilePath());
    }

    protected function store($source)
    {
        if( !imagejpeg($source, $this->getTargetPath()) ) {
            throw new \Exception( 'Failed to store resized image.' );
        }
    }
}