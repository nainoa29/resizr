<?php

namespace App\Libraries\FileProcessing;

class PngProcessor extends FileProcessor {

    protected function createSource()
    {
        return imagecreatefrompng($this->getSourceFilePath());
    }

    protected function store($source)
    {
        if( !imagepng($source, $this->getTargetPath()) ) {
            throw new \Exception( 'Failed to store resized image.' );
        }
    }
}