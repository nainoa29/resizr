<?php

namespace App\Libraries\FileProcessing;

abstract class FileProcessor {

    protected $sourceFilePath;
    protected $targetPath;

    const RESIZE_PERCENT = 0.5;

    public function __construct($sourceFilePath, $targetPath) {
        $this->sourceFilePath = $sourceFilePath;
        $this->targetPath = $targetPath;
    }

    /**
     * @return string
     */
    protected function getSourceFilePath() {
        return $this->sourceFilePath;
    }

    /**
     * @return string
     */
    protected function getTargetPath() {
        return $this->targetPath;
    }

    /**
     * @return false|\GdImage|resource
     */
    protected abstract function createSource();

    protected abstract function store($source);

    public function process() {
        try {
            $source = $this->createSource();

            list($width, $height) = getimagesize($this->getSourceFilePath());
            $newWidth = $width * self::RESIZE_PERCENT;
            $newHeight = $height * self::RESIZE_PERCENT;

            /** @var \GdImage|false|resource $thumb */
            $thumb = imagescale($source, ( int ) $newWidth, ( int ) $newHeight, IMG_BILINEAR_FIXED);

            $this->store($thumb);
        } catch(\Throwable $throwable) {
            throw $throwable;
        }
    }

}