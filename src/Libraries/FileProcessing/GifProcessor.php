<?php

namespace App\Libraries\FileProcessing;

class GifProcessor extends FileProcessor {

    protected function createSource()
    {
        return imagecreatefromgif($this->getSourceFilePath());
    }

    protected function store($source)
    {
        if( !imagegif($source, $this->getTargetPath()) ) {
            throw new \Exception( 'Failed to store resized image.' );
        }
    }
}