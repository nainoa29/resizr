<?php

namespace App\Services\Storage;

class S3StorageProvider implements StorageProviderInterface {

    private $_client;
    private $_bucket;

    /**
     * S3StorageProvider constructor.
     * @param \Aws\S3\S3Client $s3Client
     * @param $bucket
     */
    public function __construct(\Aws\S3\S3Client $s3Client, $bucket) {
        $this->_client = $s3Client;
        $this->_bucket = $bucket;
    }

    /**
     * @param StorageEntry $storageEntry
     * @return bool
     */
    public function putObject(StorageEntry $storageEntry) {
        $entry = $storageEntry->toArray();
        $entry['Bucket'] = $this->_bucket;

        $this->_client->putObject($entry);

        return true;
    }

    /**
     * @param $s3Client
     * @param $bucket
     * @return static
     */
    public static function create($s3Client, $bucket) {
        return new static($s3Client, $bucket);
    }

}