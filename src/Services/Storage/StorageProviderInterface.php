<?php

namespace App\Services\Storage;

use Throwable;

interface StorageProviderInterface {

    /**
     * @return bool
     * @throws Throwable
     */
    public function putObject(StorageEntry $storageEntry);

}