<?php

namespace App\Services\Storage;

class StorageEntry {

    protected $content;
    protected $key;
    protected $contentType;

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param mixed $contentType
     * @return StorageEntry
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return StorageEntry
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return StorageEntry
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function parse($details) {
        $this->setContentType($details['contentType'])
            ->setKey($details['key'])
            ->setContent($details['content']);
    }

    public function toArray() {
        return [
            'Key' => $this->getKey(),
            'ContentType' => $this->getContentType(),
            'Body' => $this->getContent()
        ];
    }

    public static function create() {
        return new static();
    }

}