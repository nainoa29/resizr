<?php

namespace App\Services;

use App\Requests\File;
use App\Intents\SaveImage;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Services\Storage\StorageProviderInterface;
use Slim\Psr7\UploadedFile;

class Controller {

    protected $serverRequest;
    protected $response;
    protected $storageProvider;

    protected $httpStatus = 200;

    /**
     * @param $status integer
     */
    protected function setStatus($status) {
        $this->httpStatus = $status;
    }

    /**
     * @return int
     */
    protected function getStatus() {
        return $this->httpStatus;
    }

    /**
     * Controller constructor.
     * @param ResponseInterface $response
     * @param StorageProviderInterface $storageProvider
     */
    public function __construct(ResponseInterface $response, StorageProviderInterface $storageProvider) {
        $this->response = $response;
        $this->storageProvider = $storageProvider;
    }

    public function upload(ServerRequestInterface $serverRequest) {
        try {
            $file = File::create();
            if(empty($serverRequest->getUploadedFiles())) {
                throw new \Exception('No file uploaded');
            }

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = current( $serverRequest->getUploadedFiles() );

            $fileDetails = [
                'tmp_name' => $uploadedFile->getFilePath(),
                'name' => $uploadedFile->getClientFilename(),
                'type' => $uploadedFile->getClientMediaType()
            ];

            $file->parse($fileDetails);

            SaveImage::create($this->storageProvider, $file)
                ->run();
        } catch(\Throwable $throwable) {
            $this->setStatus(422);
            $response = ['message' => $throwable->getMessage()];
        }

        return json_encode($response);
    }

    public function handle(ServerRequestInterface $serverRequest)
    {
        switch($serverRequest->getAttribute('action')){
            case 'upload':
                $result = $this->upload($serverRequest);
                break;
        }
        $response = $this->response->withStatus($this->getStatus());
        $response->getBody()->write($result);

        return $response;
    }

    /**
     * @param $request
     * @param $response
     * @param $storageProvider
     * @return static
     */
    public static function factory($response, $storageProvider) {
        return new static($response, $storageProvider);
    }

}